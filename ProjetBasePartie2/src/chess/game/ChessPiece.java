package chess.game;

import java.awt.Point;

import ChessRule.ChessRule;
import chess.ui.PieceView;
import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;

public class ChessPiece {

	// Position de la pièce sur l'échiquier
	private int gridPosX;
	private int gridPosY;

	private int type;
	private int color;

	private PieceView view;
	private ChessRule rule;

	// Pour créer des pièces à mettre sur les cases vides
	public ChessPiece(int x, int y, ChessBoard board) {
		type = ChessUtils.TYPE_NONE;
		color = ChessUtils.COLORLESS;
		gridPosX = x;
		gridPosY = y;
		rule = ChessRule.createRulesForPiece(this);
	}

	// Création d'une pièce normale. La position algébrique en notation d'échecs
	// lui donne sa position sur la grille.
	public ChessPiece(String name, String pos, ChessBoard board) {

		color = ChessUtils.getColor(name);
		type = ChessUtils.getType(name);
		view = new PieceView(type, color, board);
		rule = ChessRule.createRulesForPiece(this);
		setAlgebraicPos(pos);
	}
	
	public PieceMemento createMemento(){
		PieceMemento memento = new PieceMemento(this);
		return memento;
	}
	public ChessPiece restoreMemento(PieceMemento memento, ChessBoard board){
		gridPosX = memento.getGridX();
		gridPosY = memento.getGridY();
		type = memento.getType();
		color = memento.getColor();
		view = new PieceView(type, color, board);
		rule = ChessRule.createRulesForPiece(this);
		return this;
	}
	
	
	// Règles de mouvements
	public boolean verifyMove(Point gridPos, Point newGridPos, ChessBoard board) {
		return rule.verifyMove(gridPos, newGridPos,this, board);
	}

	public void setAlgebraicPos(String pos) {

		Point pos2d = ChessUtils.convertAlgebraicPosition(pos);

		gridPosX = pos2d.x;
		gridPosY = pos2d.y;
	}

	public int getType() {
		return type;
	}

	public int getColor() {
		return color;
	}

	public int getGridX() {
		return gridPosX;
	}

	public int getGridY() {
		return gridPosY;
	}

	public Point getGridPos() {
		return new Point(gridPosX, gridPosY);
	}

	public void setGridPos(Point pos) {
		gridPosX = pos.x;
		gridPosY = pos.y;
	}

	public Pane getUI() {
		return view.getPane();
	}

	public void moveUI(Point2D pos) {
		if (view != null)
			view.move(pos);
	}

	public boolean isNone() {

		return type == ChessUtils.TYPE_NONE;
	}

	@Override
	public boolean equals(Object other) {

		if (other instanceof ChessPiece) {
			ChessPiece otherPiece = (ChessPiece) other;

			return (color == otherPiece.color && type == otherPiece.type);
		}
		return false;

	}

}
