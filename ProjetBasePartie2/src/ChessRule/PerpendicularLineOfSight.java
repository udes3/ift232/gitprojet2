package ChessRule;

import java.awt.Point;

import chess.game.ChessBoard;
import chess.game.ChessPiece;

public class PerpendicularLineOfSight extends BasicMove{

	@Override
	public boolean verifyMove(Point gridPos, Point newGridPos, ChessPiece piece, ChessBoard board) {
		calculDistance(gridPos, newGridPos);
		if(distanceX > 0 && distanceY==0) {
			for(int i=gridPos.x + 1;i <= newGridPos.x - 1;i++) {
				if(!board.isEmpty(new Point(i,gridPos.y)))
					return false;
			}
		}
		else if(distanceX < 0 && distanceY==0) {
			for(int i=gridPos.x - 1;i >= newGridPos.x + 1;i--) {
				if(!board.isEmpty(new Point(i,gridPos.y)))
					return false;
			}
		}
		else if(distanceX==0 && distanceY > 0) {
			for(int i=gridPos.y + 1;i <= newGridPos.y - 1;i++) {
				if(!board.isEmpty(new Point(gridPos.x,i)))
					return false;
			}
		}
		else if(distanceX==0 && distanceY < 0) {
			for(int i=gridPos.y - 1;i >= newGridPos.y + 1;i--) {
				if(!board.isEmpty(new Point(gridPos.x,i)))
					return false;
			}
		}
		return true;
	}

}
