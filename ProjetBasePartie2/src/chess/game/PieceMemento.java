package chess.game;

import java.io.Writer;
import java.util.Scanner;

public class PieceMemento {
	// Position de la pi�ce sur l'�chiquier
	private int gridPosX;
	private int gridPosY;

	private int type;
	private int color;

	PieceMemento(ChessPiece piece) {
		gridPosX = piece.getGridX();
		gridPosY = piece.getGridY();
		type = piece.getType();
		color = piece.getColor();
	}

	public void saveToStream(Writer writer) throws Exception {
		
		writer.append(ChessUtils.makeAlgebraicPosition(gridPosX, gridPosY));
		writer.append('-');
		writer.append(ChessUtils.makePieceName(color, type));
		writer.append('\n');
	}

	public static ChessPiece readFromStream(Scanner reader, ChessBoard b) {
	
		String pieceDescription = reader.next();
	
		if (pieceDescription.length() != 5) {
			throw new IllegalArgumentException("Badly formed Chess Piece description: " + pieceDescription);
		}
	
		return new ChessPiece(pieceDescription.substring(3, 5), pieceDescription.substring(0, 2), b);
	
	}

	public boolean isNone() {

		return type == ChessUtils.TYPE_NONE;
	}
	public int getType() {
		return type;
	}

	public int getColor() {
		return color;
	}

	public int getGridX() {
		return gridPosX;
	}

	public int getGridY() {
		return gridPosY;
	}
}
