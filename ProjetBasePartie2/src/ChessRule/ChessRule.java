package ChessRule;

import java.awt.Point;

import chess.game.ChessBoard;
import chess.game.ChessPiece;
import chess.game.ChessUtils;

public abstract class ChessRule {
	
	// R�gles de mouvements
	public abstract boolean verifyMove(Point gridPos, Point newGridPos, ChessPiece piece, ChessBoard board);
	
	public static ChessRule and(ChessRule r1,ChessRule r2) {
		 return new AndChessRule(r1,r2);
	}
	
	public static ChessRule or(ChessRule r1,ChessRule r2) {
		 return new OrChessRule(r1,r2);
	}

	public static ChessRule createBoardRules(ChessBoard board) {
		return and(new HasPieceRule(),and(new ValidDestinationRule(),new FriendlyFireRule()));
	}
	public static ChessRule createRulesForPiece(ChessPiece piece) {
		switch (piece.getType()) {

		case ChessUtils.TYPE_PAWN:

			if (piece.getColor() == ChessUtils.WHITE) {
				return or(and(new WhitePawnBasicRule(),new EmptyDestinationRule()),
						or(and(new WhitePawnCapture(),new EnemyDestinationRule()),
								and(new WhitePawnBigStep(),and(new EmptyDestinationRule(),new PerpendicularLineOfSight()))));
			} else if (piece.getColor() == ChessUtils.BLACK) {
				return or(and(new BlackPawnBasicRule(),new EmptyDestinationRule()),
						or(and(new BlackPawnCapture(),new EnemyDestinationRule()),
								and(new BlackPawnBigStep(),and(new EmptyDestinationRule(),new PerpendicularLineOfSight()))));
			}

		case ChessUtils.TYPE_BISHOP:

			return and(new BishopBasicRule(),new DiagonalLineOfSight());

		case ChessUtils.TYPE_KING:

			return new KingBasicRule();

		case ChessUtils.TYPE_ROOK:

			return and(new RookBasicRule(),new PerpendicularLineOfSight());

		case ChessUtils.TYPE_QUEEN:

			return or(and(new BishopBasicRule(),new DiagonalLineOfSight()),
					and(new RookBasicRule(),new PerpendicularLineOfSight()));

		case ChessUtils.TYPE_KNIGHT:

			return new KnightBasicRule();
		}
		return new InvalidRule();
	}
}
