package ChessRule;

import java.awt.Point;

import chess.game.ChessBoard;
import chess.game.ChessPiece;

public class WhitePawnCapture extends BasicMove{

	@Override
	public boolean verifyMove(Point gridPos, Point newGridPos, ChessPiece piece, ChessBoard board) {
		calculDistance(gridPos, newGridPos);
		return (distanceX == 1 && distanceY == -1) || (distanceX == -1 && distanceY == -1);
	}

}
