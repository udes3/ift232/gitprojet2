package ChessRule;

import java.awt.Point;

import chess.game.ChessBoard;
import chess.game.ChessPiece;

public class BishopBasicRule extends BasicMove{

	@Override
	public boolean verifyMove(Point gridPos, Point newGridPos, ChessPiece piece, ChessBoard board) {
		calculDistance(gridPos, newGridPos);
		return (Math.abs(distanceX) == Math.abs(distanceY));

	}

}
