package chess.game;

import java.awt.Point;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class BoardMemento {
	private ArrayList<PieceMemento>pieceMementoList = new ArrayList<PieceMemento>();
	private ChessPiece[][] gridPiece;

	public BoardMemento(ChessBoard board) {
		gridPiece = board.getGrid();
		for (int i = 0; i < gridPiece.length; i++) {
			for (int j = 0; j < gridPiece[i].length; j++) {
				if(!gridPiece[i][j].isNone()) {
					PieceMemento memento= new PieceMemento(gridPiece[i][j]);
					pieceMementoList.add(memento);
				}
			}
		}
	}

	public void saveToStream(FileWriter writer) throws Exception {
		for (int i = 0; i < gridPiece.length; i++) {
			for (int j = 0; j < gridPiece[i].length; j++) {
				PieceMemento memento= new PieceMemento(gridPiece[i][j]);
				if(!memento.isNone())
					memento.saveToStream(writer);
			}
		}
	}
	
	public static ChessBoard readFromStream(Scanner reader, ChessBoard board) {
		
		while (true) {
			ChessPiece piece;
			try {
				piece = PieceMemento.readFromStream(reader, board);
			} catch (Exception e) {
				break;
			}
			board.putPiece(piece);
		}
		return board;
	}
	
	public ChessPiece getPiece(Point pos) {
		return gridPiece[pos.x][pos.y];
	}
	public ChessPiece[][] getGrid(){
		return gridPiece;
	}
	public ArrayList<PieceMemento> getList(){
		return pieceMementoList;
	}
}
