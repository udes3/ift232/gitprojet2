package ChessRule;

import java.awt.Point;

import chess.game.ChessBoard;
import chess.game.ChessPiece;

public abstract class BasicMove extends ChessRule{
	protected int distanceX;
	protected int distanceY;
	
	@Override
	public abstract boolean verifyMove(Point gridPos, Point newGridPos, ChessPiece piece, ChessBoard board);
	
	public void calculDistance(Point gridPos, Point newGridPos) {
		distanceX = newGridPos.x - gridPos.x;
		distanceY = newGridPos.y - gridPos.y;
	}

}
