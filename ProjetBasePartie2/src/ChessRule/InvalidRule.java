package ChessRule;

import java.awt.Point;

import javax.xml.crypto.NoSuchMechanismException;

import chess.game.ChessBoard;
import chess.game.ChessPiece;

public class InvalidRule extends ChessRule {

	@Override
	public boolean verifyMove(Point gridPos, Point newGridPos, ChessPiece piece, ChessBoard board) {
		throw new NoSuchMechanismException();
	}
}
