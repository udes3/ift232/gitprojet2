package ChessRule;

import java.awt.Point;

import chess.game.ChessBoard;
import chess.game.ChessPiece;

public class AndChessRule extends BasicMove{
	private ChessRule rule1;
	private ChessRule rule2;
	public AndChessRule(ChessRule r1, ChessRule r2) {
		rule1=r1;
		rule2=r2;
	}
	@Override
	public boolean verifyMove(Point gridPos, Point newGridPos, ChessPiece piece, ChessBoard board) {
		return rule1.verifyMove(gridPos, newGridPos, piece,board) && rule2.verifyMove(gridPos, newGridPos, piece,board);
	}

}
