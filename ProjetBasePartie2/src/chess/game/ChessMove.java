package chess.game;

import java.awt.Point;
import java.io.FileWriter;
import java.util.Scanner;

public class ChessMove {

	private Point depart;
	private Point arrivee;
	private BoardMemento memento;

	public ChessMove(Point pDepart, Point pArrivee,ChessBoard board) {
		depart = pDepart;
		arrivee = pArrivee;
		memento=board.createMemento();
	}
	public ChessMove(String move, ChessBoard board) {
		depart = ChessUtils.convertAlgebraicPosition(move.substring(0,2));
		arrivee = ChessUtils.convertAlgebraicPosition(move.substring(3,5));
		//memento=board.createMemento();
	}
	
	public void saveToStream(FileWriter writer) throws Exception {
		writer.append(ChessUtils.makeAlgebraicPosition(depart.x, depart.y));
		writer.append('-');
		writer.append(ChessUtils.makeAlgebraicPosition(arrivee.x, arrivee.y));
		writer.append('\n');
	}
	public Point getDepart() {
		return depart;
	}

	public Point getArrivee() {
		return arrivee;
	}
	
	public BoardMemento getBoardMemento() {
		return memento;
	}
}
