package ChessRule;

import java.awt.Point;

import chess.game.ChessBoard;
import chess.game.ChessPiece;

public class EmptyDestinationRule extends BasicMove{

	@Override
	public boolean verifyMove(Point gridPos, Point newGridPos, ChessPiece piece, ChessBoard board) {
		
		return board.isEmpty(newGridPos);
	}

}
