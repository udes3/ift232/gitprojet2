package ChessRule;

import java.awt.Point;

import chess.game.ChessBoard;
import chess.game.ChessPiece;
import chess.game.ChessUtils;

public class HasPieceRule extends ChessRule{

	@Override
	public boolean verifyMove(Point gridPos, Point newGridPos, ChessPiece piece, ChessBoard board) {
		
		return piece.getType()!=ChessUtils.TYPE_NONE;
	}

}
