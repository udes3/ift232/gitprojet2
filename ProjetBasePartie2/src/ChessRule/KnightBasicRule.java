package ChessRule;

import java.awt.Point;

import chess.game.ChessBoard;
import chess.game.ChessPiece;

public class KnightBasicRule extends BasicMove{

	@Override
	public boolean verifyMove(Point gridPos, Point newGridPos, ChessPiece piece, ChessBoard board) {
		calculDistance(gridPos, newGridPos);
		return (Math.abs(distanceX) == 1 && Math.abs(distanceY) == 2
				|| Math.abs(distanceX) == 2 && Math.abs(distanceY) == 1);
	}

}
