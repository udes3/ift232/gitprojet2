package ChessRule;

import java.awt.Point;

import chess.game.ChessBoard;
import chess.game.ChessPiece;

public class DiagonalLineOfSight extends BasicMove{
	@Override
	public boolean verifyMove(Point gridPos, Point newGridPos, ChessPiece piece, ChessBoard board) {
		calculDistance(gridPos, newGridPos);
		if(distanceX > 0 && distanceY > 0) {
			for(int i=gridPos.x + 1, j=gridPos.y + 1;i <= newGridPos.x - 1;i++,j++) {
				if(!board.isEmpty(new Point(i,j)))
					return false;
			}
		}
		else if(distanceX < 0 && distanceY > 0) {
			for(int i=gridPos.x - 1, j=gridPos.y + 1;i >= newGridPos.x + 1;i--, j++) {
				if(!board.isEmpty(new Point(i,j)))
					return false;
			}
		}
		else if(distanceX > 0 && distanceY < 0) {
			for(int i=gridPos.x + 1, j=gridPos.y - 1;i <= newGridPos.x - 1;i++, j--) {
				if(!board.isEmpty(new Point(i,j)))
					return false;
			}
		}
		else if(distanceX < 0 && distanceY < 0) {
			for(int i=gridPos.x - 1, j=gridPos.y - 1 ;i >= newGridPos.x + 1;i--, j--) {
				if(!board.isEmpty(new Point(i,j)))
					return false;
			}
		}
		return true;
	}

}
